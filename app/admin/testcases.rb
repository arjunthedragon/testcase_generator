ActiveAdmin.register Testcase do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


  permit_params :testcase_level, :number_of_testcases, :testcase_min, :testcase_max, :filename,
                input_numbers_attributes: [ :id, :input_number_position, :input_number_min, :input_number_max, :_destroy],
                array_set_properties_attributes: [ :id, :property_type, :property_value, :is_array_sorted, :_destroy]

  actions :all#, :except => [:edit, :update, :new, :destroy]

  index do
    selectable_column
    id_column
    column :testcase_level
    column :number_of_testcases
    column :testcase_min
    column :testcase_max
    
    actions
  end

  filter :testcase_level

  show do |testcase|
    attributes_table do
      row :testcase_level
      row :number_of_testcases
      row :testcase_min
      row :testcase_max

      row :input_numbers do |testcase|
        bullet_list(testcase.input_numbers.map(&:details), nil)
      end

      row :array_set_properties do |testcase|
        bullet_list(testcase.array_set_properties.map(&:details), nil)
      end 
    end

  end

  form do |f|
    f.inputs 'Testcase Template Info' do
      f.semantic_errors *f.object.errors.keys


      f.input :testcase_level, :as => :select, collection: Testcase::TESTCASE_LEVELS, selected: f.object.testcase_level.present? ? f.object.testcase_level : Testcase::TESTCASE_LEVEL_EASY, input_html: { class: "chosen-select" }
      f.input :number_of_testcases
      f.input :testcase_min
      f.input :testcase_max

      f.inputs 'Input Numbers' do
        f.has_many :input_numbers do |input_number_form|
          input_number_form.input :_destroy, :as => :boolean, :label => "Destroy?" unless input_number_form.object.new_record? 
          input_number_form.input :input_number_min
          input_number_form.input :input_number_max
          input_number_form.input :input_number_position
        end
      end

      f.inputs 'Array Set Property' do
        f.has_many :array_set_properties do |array_set_property_form|
          array_set_property_form.input :_destroy, :as => :boolean, :label => "Destroy?" unless array_set_property_form.object.new_record? 
          array_set_property_form.input :property_type, :as => :select, collection: ArraySetProperty::ARRAY_SET_PROPERTY_TYPES, selected: array_set_property_form.object.property_type.present? ? array_set_property_form.object.property_type : ArraySetProperty::ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_NORMAL, input_html: { class: "chosen-select"} 
          array_set_property_form.input :property_value
          array_set_property_form.input :is_array_sorted
        end
      end
    end
    
    f.actions
  end

  action_item :generate_testcase, :only => :show do
    link_to 'Generate Testcase', generate_testcase_admin_testcase_url(testcase), method: :post
  end

  action_item :download_testcase, :only => :show do
    link_to 'Download Testcase', download_testcase_admin_testcase_path(testcase), method: :get
  end

  member_action :generate_testcase, method: :post do
    resource.generate_testcases
    redirect_to resource_path, notice: "Test Generated (id - #{resource.id}) in File #{resource.file_name}"
  end

  member_action :download_testcase, method: :get do
    txt_filename = File.join(Rails.root, resource.file_name)
    send_file(txt_filename, :filename => resource.file_name, :disposition => 'inline', :type => "text/plain")
  end


end
