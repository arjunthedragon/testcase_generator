class ArraySetProperty < ApplicationRecord

  # => Constants ...
  ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_NORMAL = "array_size_normal"
  ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_SCALING = "array_size_scaling"
  ARRAY_SET_PROPERTY_TYPE_MULTIPLY_ARRAY_SIZES = "multiply_array_sizes"
  ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_POWER_SCALING = "array_size_power_scaling"
  ARRAY_SET_PROPERTY_TYPE_ARRAY_ELEMENT_RANGE = "array_element_range"

  ARRAY_SET_PROPERTY_TYPES = [
    ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_NORMAL,
    ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_SCALING,
    ARRAY_SET_PROPERTY_TYPE_MULTIPLY_ARRAY_SIZES,
    ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_POWER_SCALING,
    ARRAY_SET_PROPERTY_TYPE_ARRAY_ELEMENT_RANGE
  ]
  # => property type values ....
  # => array_size_normal -> "<input_number_position_1>,<input_number_position_2>..."
  # => array_size_scaling -> "<input_number_position>,<scale_factor>"
  # => multiply_array_sizes -> "<input_number_position_1>,<input_number_position_2> ....."
  # => add_array_sizes -> "<input_number_position_1>,<input_number_position_2>......"
  # => array_size_power -> "<input_number_position>,<input_number_power_scale"
  # => array_size_power_scaling -> "<input_number_position>,<power_scale_factor>"
  # => array_element_minmax -> "<input_number_position>,<min_val>,<max_val>"

  # => Validations ...
  validates :property_type, inclusion: { :in => ARRAY_SET_PROPERTY_TYPES }
  validates :property_value, presence: true

  # => Scopes ...
  scope :array_element_range_properties, -> {
    where(
      :property_type => ARRAY_SET_PROPERTY_TYPE_ARRAY_ELEMENT_RANGE
    )
  }


  def apply_property(input_numbers, array_element_range_properties_constraints_map, file)

    if self.property_type == ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_NORMAL
      array_input_sizes_idxs = property_value.split(",").map(&:to_i)# rescue raise "\n Invalid property type #{self.property_type} ->  #{self.property_value}"
    
      array_input_sizes = []
      array_input_sizes_idxs.each_with_index do | array_input_size_idx, idx|
        array_input_size = input_numbers[array_input_size_idx]
        array_element_range_min = array_element_range_properties_constraints_map[array_input_size_idx][:array_element_range_min]
        array_element_range_max = array_element_range_properties_constraints_map[array_input_size_idx][:array_element_range_max]
        
        generate_array_elements(self.is_array_sorted, array_input_size, array_element_range_min, array_element_range_max, file)
      end
    
    elsif self.property_type == ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_SCALING
      property_values = property_value.split(",").map(&:to_i)# rescue raise "\n Invalid property type #{self.property_type} ->  #{self.property_value}"
      
      array_input_size_idx = property_values.first
      scale_factor = property_values.second

      array_input_size = input_numbers[array_input_size_idx].to_i * scale_factor
      array_element_range_min = array_element_range_properties_constraints_map[array_input_size_idx][:array_element_range_min]
      array_element_range_max = array_element_range_properties_constraints_map[array_input_size_idx][:array_element_range_max]
      generate_array_elements(self.is_array_sorted, array_input_size, array_element_range_min, array_element_range_max, file)

    elsif self.property_type == ARRAY_SET_PROPERTY_TYPE_MULTIPLY_ARRAY_SIZES

      array_input_sizes_idxs = property_value.split(",").map(&:to_i)# rescue raise "\n Invalid property type #{self.property_type} ->  #{self.property_value}"
      array_input_size = array_input_sizes_idxs.map{|array_input_sizes_idx| input_numbers[array_input_sizes_idx] }.inject(:*)

      array_element_range_min = array_element_range_properties_constraints_map[array_input_sizes_idxs.first][:array_element_range_min]
      array_element_range_max = array_element_range_properties_constraints_map[array_input_sizes_idxs.first][:array_element_range_max]
      generate_array_elements(self.is_array_sorted, array_input_size, array_element_range_min, array_element_range_max, file)

    elsif self.property_type == ARRAY_SET_PROPERTY_TYPE_ARRAY_SIZE_POWER_SCALING
      property_values = property_value.split(",").map(&:to_i)# rescue raise "\n Invalid property type #{self.property_type} ->  #{self.property_value}"
      
      array_input_size_idx = property_values.first
      power_scale_factor = property_values.second

      array_input_size = input_numbers[array_input_size_idx].to_i ** power_scale_factor
      array_element_range_min = array_element_range_properties_constraints_map[array_input_size_idx][:array_element_range_min]
      array_element_range_max = array_element_range_properties_constraints_map[array_input_size_idx][:array_element_range_max]
      generate_array_elements(self.is_array_sorted, array_input_size, array_element_range_min, array_element_range_max, file)      
    end      
  end

  def generate_array_elements(is_ascending = false, array_input_size, array_element_range_min, array_element_range_max, file)
    sample_arr = []
    
    rand_num = array_element_range_min
    array_input_size.times.each do | idx |
      rand_num = rand(array_element_range_min..array_element_range_max)

      while sample_arr.include?(rand_num) == true do
        rand_num = rand(array_element_range_min..array_element_range_max)
      end

      sample_arr << rand_num

      if is_ascending == true
        sample_arr = sample_arr.sort
      end
    end

    content = ""
    sample_arr.each do |ele|
      content += " #{ele}"
    end
    content += "\n"

    content.slice!(0)

    
    #puts "\n\n content #{content}"
    file.write(content)

  end

  def details
    " #{self.property_type} -> #{self.property_value} "
  end

end
