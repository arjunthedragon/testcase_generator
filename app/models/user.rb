class User < ApplicationRecord
  include Attributes::User
  include Formats::User
  include Conditions::User
  include Actions::User

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  # => Constants ...

  ####### Audit Log #######
  AE_PW_CHANGE = 'password change'
  AUDIT_EVENTS = [AE_PW_CHANGE]
  #########################


  
  # => Relations ...
  has_many :audit_logs, :as => :auditor

  belongs_to :profile, :dependent => :destroy

  
  # => Validations ...
  validates :profile, presence: true
  validates :phone_number, presence: true, format: { with: ActiveRecordBaseCommon::Validations::VALID_PHONE_FORMAT }
  validates_uniqueness_of :phone_number

  # => Associations ...
  accepts_nested_attributes_for :profile
  
  
  # => Callbacks ...  
  before_save do
    if self.authentication_token.blank?
      self.authentication_token = generate_new_auth_token
    end
  end

end
