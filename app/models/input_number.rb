class InputNumber < ApplicationRecord

  # => Relations ...
  belongs_to :testcase

  # => Validations ...
  validates :input_number_position, presence: true
  validates :input_number_min, :input_number_max, presence: true

  validates_uniqueness_of :input_number_position, scope: :testcase_id


  def details
    " #{self.input_number_position} -> #{self.input_number_min} -> #{self.input_number_max} "
  end

end
