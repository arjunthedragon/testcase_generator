class Testcase < ApplicationRecord
  include ActiveRecordBaseCommon::Validations

  # => Constants ...
  TESTCASE_LEVEL_EASY = "easy"
  TESTCASE_LEVEL_MEDIUM = "medium"
  TESTCASE_LEVEL_HARD = "hard"

  TESTCASE_LEVELS = [
    TESTCASE_LEVEL_EASY,
    TESTCASE_LEVEL_MEDIUM,
    TESTCASE_LEVEL_HARD
  ]

  # => Relations ...
  has_many :array_set_properties, dependent: :destroy
  has_many :input_numbers, dependent: :destroy

  # => validations
  validates :testcase_min, :testcase_max, presence: true
  validates :input_numbers, has_many: true
  validates :array_set_properties, has_many: true

  validates :testcase_level, inclusion: { :in => TESTCASE_LEVELS }
  validates_uniqueness_of :file_name

  # => Callbacks ...
  before_validation on: :create do 
    if self.number_of_testcases.nil?
      self.number_of_testcases = rand(self.testcase_min..self.testcase_max)
    end

    #self.number_of_inputs = self.input_numbers.count rescue 0
  end

  # => Nested Attributes ...
  accepts_nested_attributes_for :input_numbers, :allow_destroy => true
  accepts_nested_attributes_for :array_set_properties, :allow_destroy => true

  #after_save :generate_testcases

  # => Accessors ..
  #attr_accessor :number_of_inputs

  def array_element_range_properties_constraints_map
    constraints_map = {}

    array_element_range_properties = self.array_set_properties.array_element_range_properties
    
    array_element_range_properties.each do |array_element_range_property|
      property_values = array_element_range_property.property_value.split(",").map(&:to_i)
      
      input_number_position = property_values[0]
      array_element_range_min = property_values[1]
      array_element_range_max = property_values[2]

      constraints_map[input_number_position] = {
        :array_element_range_min => array_element_range_min,
        :array_element_range_max => array_element_range_max
      }
    end

    constraints_map
  end

  def difficult_testcases_range
    testcases_range = nil

    if self.number_of_testcases > 2
      number_of_difficult_testcases = 0
      if self.testcase_level == TESTCASE_LEVEL_MEDIUM
        number_of_difficult_testcases = self.number_of_testcases / 4
      elsif self.testcase_level == TESTCASE_LEVEL_HARD
        number_of_difficult_testcases = self.number_of_testcases / 2
      end

      testcases_range = (testcase_max - number_of_difficult_testcases)..(self.testcase_max) if number_of_difficult_testcases > 0
    end
  end

  def generate_testcases
    
    self.file_name = self.file_name || "TestcaseID_#{self.id}.txt"
    self.save!

    File.delete(self.file_name) if File.exist?(self.file_name)

    file = File.open(self.file_name, "a")
    
    content = ""
    
    content += "#{self.number_of_testcases}\n"
    file.write(content)

    difficult_testcases_range_var = difficult_testcases_range

    self.number_of_testcases.times do |testcase_idx|
      
      input_nums = []
      

      content = ""
      self.input_numbers.each do |input_number|
        
        if difficult_testcases_range_var.present? && difficult_testcases_range_var.include?(testcase_idx + 1)
          input_num = input_number.input_number_max
        else
          input_num = rand(input_number.input_number_min..input_number.input_number_max)
        end

        input_nums << input_num


        content += " #{input_num}"
      end

      content.slice!(0)
      content += "\n"

      
      file.write(content)

      
      array_element_range_properties_constraints_map_var = array_element_range_properties_constraints_map
      
      self.array_set_properties.each do |array_set_property|
      	array_set_property.apply_property(input_nums, array_element_range_properties_constraints_map_var, file)
      end
    end

    file.close
  end

end
