Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: 'static_pages#home'

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resource :static_pages, only: [] do
    collection do
      get :home
    end
  end

  resources :users, only: [:index, :show, :destroy, :update, :edit] do
  end

end