class CreateInputNumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :input_numbers do |t|
      t.references :testcase, index: true

      t.integer :input_number_position, index: true

      t.integer :input_number_min, default: 1
      t.integer :input_number_max, default: 1000


      t.timestamps
    end

    add_index :input_numbers, [:input_number_position, :testcase_id], unique: true
  end
end
