class CreateArraySetProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :array_set_properties do |t|
      t.references :testcase, index: true

      t.string :property_type, index: true
      t.string :property_value

      t.boolean :is_array_sorted, default: false

      t.timestamps
    end
  end
end
