class CreateTestcases < ActiveRecord::Migration[5.0]
  def change
    create_table :testcases do |t|
      t.string :testcase_level, index: true

      t.integer :number_of_testcases
      t.integer :testcase_min, default: 1
      t.integer :testcase_max, default: 10

      t.string :file_name


      t.timestamps
    end
  end
end
